const passport = require('passport');
const GoogleStrategy = require('passport-facebook').Strategy;
const strategy_name = 'google';
require('dotenv').config()

passport.use(strategy_name, new GoogleStrategy({
    clientID: process.env.client_Id_FB,
    clientSecret: process.env.Client_Secret_FB,
    callbackURL: process.env.FB_CALLBACK,
    profileFields: ['id', 'emails', 'name'],
  },
  function(accessToken, refreshToken, profile, done) {
    // User.findOrCreate({ googleId: profile.id }, function (err, user) {
    //   return done(err, user);
    // });
    return done(null, profile);
  }
));